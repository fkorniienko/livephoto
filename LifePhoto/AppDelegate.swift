//
//  AppDelegate.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Injection.factory.registerServices()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let navigationController = NavigationController.newInstance()
        navigationController.setViewControllers([HomeViewController.newInstance()], animated: false)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        return true
    }
    
}

