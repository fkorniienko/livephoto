//
//  Carousel.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import UIKit

// MARK: - CarouselItem
final class CarouselItem {
    
    let view: UIView
    
    init(inboundView: UIView) {
        view = inboundView
    }
    
}

// MARK: - Carousel Delegate protocol
@objc protocol CarouselDelegate {
    
    func didSelectItem(at index: Int)
    func didPress(on item: UIView)
    func didEndPress(on item: UIView)
    func didEndAnimation(on item: UIView)
    func didBeginAnimation(on item: UIView)
}

// MARK: - Carousel DataSource protocol
@objc protocol CarouselDataSource {
    
    func numberOfItems() -> Int
    func viewForItem(atIndex viewIndex: Int) -> UIView
    func itemSize() -> CGSize
    
}

// MARK: - Carousel
@IBDesignable final class Carousel: UIView {
    
    // MARK: - Private variables
    private var velocityLimit: CGFloat = 200
    private var decelerationTime: TimeInterval = 0.3
    private var rotation: CGFloat = 0
    private var animations: Int = 0 {
        didSet {
            update()
        }
    }
    private var radius: CGFloat = 0 {
        didSet {
            updateDisplay()
        }
    }
    private var circumference: Double = 0
    private var itemSize = CGSize(width: 0, height: 0)
    lazy private var panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panned))
    lazy private var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
    lazy private var longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(pressed))
    
    // MARK: - Public variables
    var items = [CarouselItem]()
    @IBInspectable var seperation: CGFloat = 0 {
        didSet {
            updateRadius()
        }
    }
    @IBOutlet weak var delegate: CarouselDelegate?
    @IBOutlet weak var dataSource: CarouselDataSource? {
        didSet {
            resetAndAddViews()
        }
    }
    private let transformLayer: CATransformLayer = {
        var perspectiveTransform = CATransform3DIdentity
        perspectiveTransform.m34 = -1/1000
        let transformLayer = CATransformLayer()
        transformLayer.transform = perspectiveTransform
        return transformLayer
    }()
    var isDoubleSided: Bool = true {
        didSet {
            resetAndAddViews()
        }
    }
    
    // MARK: - Initialization methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        initCustom()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initCustom()
    }
    
    private func initCustom() {
        layer.addSublayer(transformLayer)
        transformLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        translatesAutoresizingMaskIntoConstraints = false
        isUserInteractionEnabled = true
        addGestureRecognizers()
    }
    
    private func addGestureRecognizers() {
        panGestureRecognizer.minimumNumberOfTouches = 1
        panGestureRecognizer.maximumNumberOfTouches = 1
        panGestureRecognizer.isEnabled = false
        addGestureRecognizer(panGestureRecognizer)
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.isEnabled = true
        addGestureRecognizer(tapGestureRecognizer)
        longGestureRecognizer.isEnabled = true
        addGestureRecognizer(longGestureRecognizer)
    }
    
    // MARK: - User interaction methods
    @objc private func panned(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self)
        let velocity = recognizer.velocity(in: self)
        switch recognizer.state {
        case UIGestureRecognizer.State.began:
            fallthrough
        case UIGestureRecognizer.State.changed:
            rotateForTranslation(translation: translation)
        case UIGestureRecognizer.State.ended:
            animateDeceleration(translation: translation, velocity: velocity)
        default:
            break
        }
        recognizer.setTranslation(CGPoint.zero, in: self)
    }
    
    @objc private func tapped(_ recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case UIGestureRecognizer.State.ended:
            let tapPoint = recognizer.location(in: self)
            if tapPoint.x > (center.x + (itemSize.width / 2)) {
                scrollToPrevious()
            } else if tapPoint.x < (center.x - (itemSize.width / 2)) {
                scrollToNext()
            } else {
                selectCurrentItem()
            }
        default:
            break
        }
    }
    
    @objc private func pressed(_ recognizer: UITapGestureRecognizer) {
        switch recognizer.state {
        case .began:
            delegate?.didPress(on: items[currentItemIndex()].view)
            
        case .ended:
            delegate?.didEndPress(on: items[currentItemIndex()].view)
        default:
            break
        }
    }
    
    private func update() {
        if animations == 0 {
            delegate?.didEndAnimation(on: items[currentItemIndex()].view)
        } else {
            delegate?.didBeginAnimation(on: items[currentItemIndex()].view)
        }
    }
    
    func currentItemIndex() -> Int {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return 0 }
        let seperationAngle = CGFloat((2 * Double.pi) / Double(numberOfItems))
        let remainder = rotation.remainder(dividingBy: seperationAngle)
        let unbalancedIndex = ((rotation - remainder) / seperationAngle).rounded(.toNearestOrEven)
        var index = Int(unbalancedIndex.truncatingRemainder(dividingBy: CGFloat(numberOfItems)))
        index *= -1
        if index < 0 {
            index += numberOfItems
        }
        return index
    }
    
    private func selectCurrentItem() {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return }
        guard animations < 1 else { return }
        if let delegate = delegate {
            delegate.didSelectItem(at: currentItemIndex())
        }
    }
    
    private func scrollToNext() {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return }
        let seperationAngle = CGFloat((2 * Double.pi) / Double(numberOfItems))
        rotation += CGFloat(seperationAngle / 2)
        animateToSnappingPoint(angle: Double(seperationAngle))
    }
    
    private func scrollToPrevious() {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return }
        let seperationAngle = CGFloat((2 * Double.pi) / Double(numberOfItems))
        rotation -= CGFloat(seperationAngle / 2)
        animateToSnappingPoint(angle: Double(-seperationAngle))
    }
    
    private func animateToSnappingPoint(angle: Double) {
        let numberOfItems = items.count
        let seperationAngle = CGFloat((2 * Double.pi) / Double(numberOfItems))
        let reminder = rotation.remainder(dividingBy: seperationAngle)
        if angle > 0 {
            if reminder > 0 {
                rotation += seperationAngle - reminder
            } else {
                rotation -= reminder
            }
        } else {
            if reminder > 0 {
                rotation -= reminder
            } else {
                rotation -= seperationAngle + reminder
            }
        }
        animations += 1
        UIView.animate(withDuration: decelerationTime,
                       delay: 0,
                       options: [.allowAnimatedContent, .beginFromCurrentState, .curveEaseOut],
                       animations: {
                        self.updateDisplay()
        },
                       completion: { (completed: Bool) in
                        self.animations -= 1
        })
    }
    
    private func rotateForTranslation(translation: CGPoint) {
        let angle = (Double.pi * 2 * Double(translation.x)) / circumference
        self.rotation += CGFloat(angle)
        updateDisplay()
    }
    
    private func animateDeceleration(translation: CGPoint, velocity: CGPoint) {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return }
        var limitedVelocity = CGPoint(x: velocity.x, y: velocity.y)
        if velocity.x > velocityLimit {
            limitedVelocity.x = velocityLimit
        }
        if velocity.x < -velocityLimit {
            limitedVelocity.x = -velocityLimit
        }
        let angle = (Double.pi * 2 * Double(translation.x + (limitedVelocity.x * CGFloat(decelerationTime)))) / circumference
        animateToSnappingPoint(angle: angle)
    }
    
    // MARK: - Presentation Methods
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        resetAndAddViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        transformLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        updateDisplay()
    }
    
    private func updateRadius() {
        var itemSeperationAddition: Double = 0
        let count = items.count
        var newRadius: Double = 0
        if count > 0 {
            newRadius = Double(itemSize.width) / (2 * sin(Double.pi / Double(count)))
            itemSeperationAddition = Double(seperation) * Double(count)
        }
        circumference = Double(newRadius) * 2 * Double.pi + itemSeperationAddition
        newRadius = circumference / (2 * Double.pi)
        radius = CGFloat(newRadius)
    }
    
    private func updateDisplay() {
        let numberOfItems = items.count
        guard numberOfItems > 0 else { return }
        let seperationAngle = (2 * Double.pi) / Double(numberOfItems)
        for (index, item) in items.enumerated() {
            let view = item.view
            var transform: CATransform3D = CATransform3DIdentity
            transform = CATransform3DTranslate(transform, 0, 0, -radius)
            transform = CATransform3DRotate(transform, self.rotation + CGFloat(seperationAngle * Double(index)), 0, 1, 0)
            transform = CATransform3DTranslate(transform, 0, 0, radius)
            view.layer.transform = transform
        }
    }
    
    private func resetAndAddViews() {
        for item in items {
            let itemView = item.view
            itemView.layer.removeFromSuperlayer()
        }
        items.removeAll()
        guard let dataSource = dataSource else {
            panGestureRecognizer.isEnabled = false
            return
        }
        let numberOfItems = dataSource.numberOfItems()
        if numberOfItems > 0 {
            panGestureRecognizer.isEnabled = true
        }
        itemSize = dataSource.itemSize()
        
        for index in 0..<numberOfItems {
            let itemView = dataSource.viewForItem(atIndex: index)
            itemView.layer.isDoubleSided = isDoubleSided
            let item = CarouselItem(inboundView: itemView)
            items.append(item)
            self.addSubview(itemView)
            itemView.frame = CGRect(x: -(itemSize.width / 2),
                                    y: -(itemSize.height / 2),
                                    width: itemSize.width,
                                    height: itemSize.height)
            itemView.setNeedsLayout()
            itemView.setNeedsDisplay()
            itemView.layoutIfNeeded()
            itemView.layoutSubviews()
            transformLayer.addSublayer(itemView.layer)
        }
        updateRadius()
    }
    
}

// MARK:- Public methods to control carousel
extension Carousel {
    
    func reload() {
        resetAndAddViews()
    }
    
}
