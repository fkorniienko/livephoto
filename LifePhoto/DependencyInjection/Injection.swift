//
//  Injection.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import Foundation

enum InjectableProtocols: String {
    case homeViewModelProtocol = "HomeViewModelProtocol"

    static let allItems: [InjectableProtocols] = [.homeViewModelProtocol]
}

final class Injection {

    private var services = [String: (() -> AnyObject)]()

    static var factory = Injection()
    private init() { }
    func registerServices() {
        InjectableProtocols.allItems.forEach { (service: InjectableProtocols) in
            switch service {
            case .homeViewModelProtocol:
                services[service.rawValue] = {
                    return HomeViewModel()
                }
            }
        }
    }

    func inject(service: InjectableProtocols) -> AnyObject {
        services[service.rawValue]!()
    }
}
