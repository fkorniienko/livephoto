//
//  Photos.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import Foundation

struct Photos {
    let image: String?
    let video: String?
    init(nameImg: String, typeImg: String, nameVideo: String, typeVideo: String) {
        image = Bundle.main.path(forResource: nameImg, ofType: typeImg)
        video = Bundle.main.path(forResource: nameVideo, ofType: typeVideo)
    }
    static func setupDummyData() -> [Photos] {
        return [
            Photos(nameImg: "0", typeImg: "jpg", nameVideo: "1", typeVideo: "mov"),
            Photos(nameImg: "2", typeImg: "jpg", nameVideo: "3", typeVideo: "mov")
        ]
    }
}
