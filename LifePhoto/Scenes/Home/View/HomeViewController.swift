//
//  HomeViewController.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import UIKit

final class HomeViewController: BaseViewController {
    
    static func newInstance(inboundViewModel: HomeViewModelProtocol? = nil) -> HomeViewController {
        return HomeViewController(nibName: String(describing: HomeViewController.self), bundle: nil)
    }
    
    private var viewModel: HomeViewModelProtocol = Injection.factory.inject(service: InjectableProtocols.homeViewModelProtocol) as! HomeViewModelProtocol
    
    @IBOutlet private weak var carousel: Carousel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        UIStatusBarStyle.lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save,
                                                            target: self,
                                                            action: #selector(save))
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupData()
    }
    
    private func setupData() {
        viewModel.getPhotos { [weak self] (result: Result<[Photos], Error>) in
            self?.carousel.reload()
            switch result {
            case .failure(let error): debugPrint(error)
            case .success(_): break
            }
        }
    }
    
   @objc private func save() {
        let data = viewModel.photos[carousel.currentItemIndex()]
        LifePhotoManager.makeLivePhotoFromItems(imageURL: data.image ?? "", videoURL: data.video ?? "") { (error) in
            debugPrint(error as Any)
        }
    }
}

extension HomeViewController: CarouselDelegate {
    
    func didSelectItem(at index: Int) { }
    
    func didPress(on item: UIView) {
        if let currentView = item as? HomeCarouselCell {
            currentView.addVideo()
        }
    }
    
    func didEndPress(on item: UIView) {
        if let currentView = item as? HomeCarouselCell {
            currentView.removePlayer()
        }
    }
    
    func didEndAnimation(on item: UIView) {
        if let currentView = item as? HomeCarouselCell {
            currentView.addVideo()
        }
    }
    
    func didBeginAnimation(on item: UIView) {
        if let currentView = item as? HomeCarouselCell {
            currentView.removePlayer()
        }
    }
}

extension HomeViewController: CarouselDataSource {
    
    func numberOfItems() -> Int {
        return viewModel.photos.count
    }
    
    func itemSize() -> CGSize {
        return HomeCarouselCell.size
    }
    
    func viewForItem(atIndex viewIndex: Int) -> UIView {
        if viewIndex < viewModel.photos.count {
            return HomeCarouselCell.newCell(photo: viewModel.photos[viewIndex])
        } else {
            return HomeCarouselCell.newCell()
        }
    }
}
