//
//  HomeCarouselCell.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import UIKit
import AVKit

final class HomeCarouselCell: UIView {
    
    @IBOutlet private weak var photoIV: UIImageView!
    private var player: AVPlayer?
    private var playerVC: AVPlayerViewController?
    private var data: Photos?
    
    private func set(photo: Photos) {
        guard let img = photo.image else { return }
        photoIV.image = UIImage(contentsOfFile: img )
        data = photo
    }
    
    @objc func addVideo() {
        guard let str = data?.video, playerVC == nil else { return }
        player = AVPlayer(url: URL(fileURLWithPath: str))
        playerVC = AVPlayerViewController()
        playerVC?.player = player
        playerVC?.view.frame = photoIV.frame
        playerVC?.view.backgroundColor = .clear
        playerVC?.showsPlaybackControls = false
        guard let playerView = playerVC?.view else { return }
        addSubview(playerView)
        NotificationCenter.default.addObserver(self, selector: #selector(removePlayer), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        player?.play()
    }
    
    @objc func removePlayer() {
        playerVC?.view.removeFromSuperview()
        player = nil
        playerVC = nil
    }
}

extension HomeCarouselCell {
    
    static var size: CGSize {
        if let homeCarouselCell = Bundle.main.loadNibNamed(String(describing: HomeCarouselCell.self),
                                                           owner: nil,
                                                           options: [UINib.OptionsKey : Any]())?.first as? HomeCarouselCell {
            return homeCarouselCell.bounds.size
        } else {
            return CGSize.zero
        }
    }
    
    static func newCell() -> HomeCarouselCell {
        if let homeCarouselCell = Bundle.main.loadNibNamed(String(describing: HomeCarouselCell.self),
                                                           owner: nil,
                                                           options: [UINib.OptionsKey : Any]())?.first as? HomeCarouselCell {
            return homeCarouselCell
        } else {
            return HomeCarouselCell()
        }
    }
    static func newCell(photo: Photos) -> HomeCarouselCell {
        if let homeCarouselCell = Bundle.main.loadNibNamed(String(describing: HomeCarouselCell.self),
                                                           owner: nil,
                                                           options: [UINib.OptionsKey : Any]())?.first as? HomeCarouselCell {
            homeCarouselCell.set(photo: photo)
            return homeCarouselCell
        } else {
            return HomeCarouselCell()
        }
    }
}
