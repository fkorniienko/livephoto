//
//  HomeViewModelProtocol.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import Foundation

protocol HomeViewModelProtocol {

    var photos: [Photos] { get set }
    func getPhotos(completion: @escaping (Result<[Photos], Error>) -> ())
}
