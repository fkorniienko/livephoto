//
//  HomeViewModel.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import Foundation

final class HomeViewModel: HomeViewModelProtocol {
    var photos: [Photos] = []
    
    func getPhotos(completion: @escaping (Result<[Photos], Error>) -> ()) {
        photos = Photos.setupDummyData()
        completion(.success(photos))
    }
}
