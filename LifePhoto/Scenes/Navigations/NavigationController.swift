//
//  NavigationController.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    
    static func newInstance() -> NavigationController {
        NavigationController(nibName: String(describing: NavigationController.self), bundle: nil)
    }
}
