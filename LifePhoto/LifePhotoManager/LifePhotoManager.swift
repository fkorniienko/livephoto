//
//  LifePhotoManager.swift
//  LifePhoto
//
//  Created by Fedir Korniienko on 1/15/20.
//  Copyright © 2020 Fedir. All rights reserved.
//

import Foundation
import Photos

final class LifePhotoManager {
    static func makeLivePhotoFromItems(imageURL: String, videoURL: String,
                                       finichCallack: @escaping (_ livePhoto: Error?) -> Void) {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                LivePhotoGenerator.create(inputImagePath: imageURL, inputVideoPath: videoURL) { (livePhoto: LivePhoto?, error: Error?) in
                    if let livePhoto = livePhoto {
                        livePhoto.writeToPhotoLibrary(completion: { (livePhoto: LivePhoto, error: Error?) in
                            finichCallack(error)
                        })
                    }
                }
            default: break
            }
        }
    }
}
